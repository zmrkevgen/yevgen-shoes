package oop;

import ShoesBox.*;

public abstract class Main {

    public static void main(String[] args) {
        Nike nike = new Nike();
        Adidas adidas = new Adidas();
        Reebok reebok = new Reebok();
        ShoesChecker shoesChecker = new ShoesChecker();
        PriceComparing priceComparing = new PriceComparing();

        System.out.println("Nike shoes properties");
        nike.setNikeCol(Nike.NikeColors.WHITE);
        System.out.println(nike.getNikeCol());
        nike.setNikeMat(Nike.nikeMaterial.SYNTETIC);
        System.out.println(nike.getMaterial());

        System.out.println("Adidas shoes properties");
        adidas.setAdidasCol(Adidas.AdidasColors.BLUE);
        System.out.println(adidas.getNikeCol());
        adidas.setAdidasMat(Adidas.adidasMaterial.LEATHER);
        System.out.println(adidas.getMaterial());

        System.out.println("Reebok shoes properties");
        reebok.setReebokCol(Reebok.ReebokColors.PURPLE);
        System.out.println(reebok.getReebokCol());
        reebok.setReebokMat(Reebok.reebokMaterial.CLOTH);
        System.out.println(reebok.getMaterial());

        System.out.println("Total sum of all shoes");
        System.out.println(Nike.COST_NIKE + Adidas.ADIDAS_COST + Reebok.REEBOK_COST);

        System.out.println("Shoes labels");
        nike.label();
        adidas.label();
        reebok.label();

        System.out.println("Price sorting");
        shoesChecker.counter();

        System.out.println("Shoes prices");
        adidas.dipsplayInfo();
        nike.dipsplayInfo();
        reebok.dipsplayInfo();

        System.out.println("The most cheap shoes is cost");
        priceComparing.MinPrice();




    }
}
