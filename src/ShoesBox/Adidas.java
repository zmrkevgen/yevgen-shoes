package ShoesBox;

import oop.Labels.ShoesLabel;

public class Adidas extends ShoesPrice implements ShoesLabel {
    public AdidasColors colors;
    public adidasMaterial material;
    public  static final int ADIDAS_COST = 3000;
    public void setAdidasCol (AdidasColors colors){
        this.colors=colors;
    }
    public AdidasColors getNikeCol(){
        return  colors;
    }

    @Override
    public void label() {
        System.out.println("Impossible is nothing");
    }

    @Override
    public void dipsplayInfo() {
        System.out.println("Adidas shoes is costs "+ADIDAS_COST);
    }

    public static enum AdidasColors{
        GREEN("green"), YELLOW("yellow"),BLUE("blue");
        private final String colors;

        AdidasColors (String colors){
            this.colors=colors;
        }
    }
    public void setAdidasMat(adidasMaterial material){
        this.material=material;
    }

    public adidasMaterial getMaterial() {
        return material;
    }
    public static enum adidasMaterial{
        LEATHER("leather"), CLOTH("cloth"), SYNTETIC("syntetic");
        private final String material;

        adidasMaterial(String material){
            this.material=material;
        }
    }
}
