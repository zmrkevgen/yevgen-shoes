package ShoesBox;

import oop.Labels.ShoesLabel;

public class Nike extends ShoesPrice implements ShoesLabel {
    public NikeColors colors;
    public  nikeMaterial material;
    public  static  final int COST_NIKE = 2700;

    public void setNikeCol (NikeColors colors){
        this.colors=colors;
    }
    public NikeColors getNikeCol(){
        return  colors;
    }

    @Override
    public void label() {
        System.out.println("Just do it");
    }

    @Override
    public void dipsplayInfo() {
        System.out.println("Nike shoes is costs "+COST_NIKE);
    }

    public static enum NikeColors{
        WHITE("white"), BLACK("black"), RED("red");
        private final String colors;

        NikeColors (String colors){
            this.colors=colors;
        }
    }
    public void setNikeMat(nikeMaterial material){
        this.material=material;
    }

    public nikeMaterial getMaterial() {
        return material;
    }
    public static enum nikeMaterial{
        LEATHER("leather"), CLOTH("cloth"), SYNTETIC("syntetic");
        private final String material;
        nikeMaterial(String material){
            this.material=material;
        }
    }
}
