package ShoesBox;

import oop.Labels.ShoesLabel;

public class Reebok extends ShoesPrice implements ShoesLabel {
    public ReebokColors colors;
    public  reebokMaterial material;
    public static final int REEBOK_COST = 2500;

    public void setReebokCol (ReebokColors colors){
        this.colors=colors;
    }
    public ReebokColors getReebokCol(){
        return  colors;
    }

    @Override
    public void label() {
        System.out.println("I am what i am");
    }

    @Override
    public void dipsplayInfo() {
        System.out.println("Reebok shoes is costs "+REEBOK_COST);
    }

    public static enum ReebokColors{
        GREY("grey"), PINK("pink"), PURPLE("purple");
        private final String colors;

        ReebokColors (String colors){
            this.colors=colors;
        }
    }
    public void setReebokMat(reebokMaterial material){
        this.material=material;
    }

    public reebokMaterial getMaterial() {
        return material;
    }
    public static enum reebokMaterial{
        LEATHER("leather"), CLOTH("cloth"), SYNTETIC("syntetic");
        private final String material;
        reebokMaterial(String material){
            this.material=material;
        }
    }
}
